
//
//  ViewController.swift
//  SampleBrowseVideoInPhotoLibrary
//
//  Created by Arthit Thongpan on 8/18/16.
//  Copyright © 2016 Arthit Thongpan. All rights reserved.
//

import UIKit
import Photos
import MobileCoreServices

class ViewController: UIViewController {
    
    @IBOutlet weak var detailDescriptionLabel: UILabel!
    @IBOutlet weak var imageView: UIImageView!
    
    let imageManager = PHCachingImageManager()
    
    var asset: PHAsset? {
        didSet {
            // Update the view.
            self.configureView()
        }
    }
    
    func configureView() {
        // Update the user interface for the detail item.
        if let asset = self.asset {
            if let label = self.detailDescriptionLabel {
                label.text = asset.localIdentifier
            }
            
            if let imageView = self.imageView {
                
                let options = PHImageRequestOptions()
                options.resizeMode = .Exact;
                
                let retinaMultiplier = UIScreen.mainScreen().scale
                let retinaSquare = CGSizeMake(imageView.bounds.size.width * retinaMultiplier, imageView.bounds.size.height * retinaMultiplier);
                imageManager.requestImageForAsset(asset, targetSize: retinaSquare, contentMode: .AspectFill, options: options) { (image, _) in
                    
                    dispatch_async(dispatch_get_main_queue(), { () -> Void in
                        imageView.image = UIImage(CGImage: (image?.CGImage)!, scale: retinaMultiplier, orientation: (image?.imageOrientation)!)
                        self.view.setNeedsLayout()
                        self.view.layoutIfNeeded()
                    })
                }
            }
        }
    }
   
    @IBAction func browseFile(sender: AnyObject) {
        if UIImagePickerController.isSourceTypeAvailable(.SavedPhotosAlbum) == false {
            return
        }

        let mediaUI = UIImagePickerController()
        mediaUI.sourceType = .SavedPhotosAlbum
        let mediaTypes: Array<String> = [(kUTTypeMovie as String)]
        mediaUI.mediaTypes = mediaTypes
        mediaUI.allowsEditing = true
        mediaUI.delegate = self
        
        presentViewController(mediaUI, animated: true, completion: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.configureView()
    }
}

// MARK: - UIImagePickerControllerDelegate

extension ViewController: UIImagePickerControllerDelegate {
    
    func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : AnyObject]) {
        let mediaType = info[UIImagePickerControllerMediaType] as! NSString
        dismissViewControllerAnimated(true) { 
            if mediaType == kUTTypeMovie {

//                let image = info[UIImagePickerControllerOriginalImage] as! UIImage
                // Assets Library framework
                let url = info[UIImagePickerControllerReferenceURL] as! NSURL
                
                let fetchResult = PHAsset.fetchAssetsWithALAssetURLs([url], options: nil)
                if let asset = fetchResult.firstObject{
                    self.asset = asset as! PHAsset
                }
                
            }
        }
    }

}

// MARK: - UINavigationControllerDelegate

extension ViewController: UINavigationControllerDelegate {
    
}


